const express = require('express')
const app = express();
const YAML = require('yamljs')
const { connector } = require('swagger-routes-express')
const api = require('./index')
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = YAML.load('./documentation/api.yml');


const connect = connector(api, swaggerDocument)

connect(app) // attach the routes

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

const port = process.env.PORT || 3002;
app.listen(port, () => console.log(`Documentation Server listening on port ${port}...`));


