const chai = require("chai");
const AuthServer = require("../authentication").app;
const chaiHttp = require("chai-http");
const bodyParser = require('body-parser');

chai.should();
chai.use(chaiHttp);

function between(min, max) {  
    return Math.floor(
      Math.random() * (max - min + 1) + min
    )
  };

const testId = between(10,20);
var userInfo = {
    userId: null,
    accessToken: null,
    refreshToken: null
};


describe('Test authenticaiton', () => {
    /**
     * Test /login
     */

    describe("POST /login", () => {
        
        it("it should return a message, id, name, pwd and two tokens", (done) => {
            let body = {
                userId: testId,
                userPassword: "anypwd"
            }
            chai.request(AuthServer)
                .post('/login')
                .send(body)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message', "Account created.");
                    userInfo['userId'] = testId;
                    userInfo['accessToken'] = res.body.accessToken;
                    userInfo['refreshToken'] = res.body.refreshToken;
                done();
                })
        })
        it("it should return 404 after post a wrong route", (done) => {
            chai.request(AuthServer)
                .post("/api/login")
                .end((err, res) => {
                    res.should.have.status(404);
                done();
                })
        })
    });
    describe("POST /logout", () => {
        it("it should return nothing", (done) => {

            chai.request(AuthServer)
                .delete('/logout')
                .send(userInfo)
                .end((err, res) => {
                    res.should.have.status(204);
                done();
                })
        })

    });
})