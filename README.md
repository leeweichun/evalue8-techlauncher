# Hi Evaluation Panel!

* To check our documentation, go to our [Project Wiki Page](https://gitlab.com/leeweichun/evalue8-techlauncher/-/wikis/home).
* To track our progress, explore the [Boards under Issues](https://gitlab.com/leeweichun/evalue8-techlauncher/-/boards).

# CSV Processing API Overview
The main functionality of this API is to parse CSV/XLSX files and inputs extracted data into the Evalue8 database.

# How To Use
* Requires the servers to be up and running. View the Set up section for instructions.
* To start up the servers, simply do
```
node index.js
```

Send POST request to http://[url_here]:5000/login with a raw object which contains the following key-value pairs:
* userId: [your userId]
* userPassword: [your userPassword]
If kv pairs offered correctly, the API will immediately respond to the request with a JSON String containing an accessToken and a refreshToken you will need for the next step.

Send a POST request to http://[url_here]:3001/upload with a multipart/form-data object which contains the following key-value pairs:
* sampleFile: [your csv/xlsx file]
* userId: [your userId]
* accessToken: [your accessToken]
* refreshToken: [your refreshToken]

If successful, the API will immediately respond to the request with k/v pairs from the CSV/XLSX file.

# Authentication Setup
1. MySQL Setup
   * Windows: https://dev.mysql.com/doc/refman/8.0/en/windows-installation.html
   * Linux: https://dev.mysql.com/doc/mysql-linuxunix-excerpt/8.0/en/linux-installation.html
   * Mac: https://dev.mysql.com/doc/mysql-osx-excerpt/5.7/en/osx-installation-pkg.html

2. Open MySQL Workbench and run the provided SQL script to build the database and the authentication table `users`
   Check if the database and table is successfully created using: 
    ```
    USE apiDatabase;
    SELECT * FROM users;
    ```

3. Run index.js. If anything doesn’t work make sure to have below installed/executed:
    ```
	npm i express jsonwebtoken dotenv
	npm i --save-dev nodemon
    ```

# Detailed instructions and documentation
Please follow this link for detailed documentation and setup guides:
https://drive.google.com/drive/folders/1fgVn6bZ5fYFJ-7nJWou5sQSmC2VxgiUh?usp=sharing




