require("dotenv").config();

const express = require("express");
const app = express();

const fileUpload = require("express-fileupload");
const xlsx = require("node-xlsx");
const pool = require("../evalue8-techlauncher/database");
const observationFields =
  "id, observation_template_version_id, assessment_activity_id, start_date, end_date, created_by_id, created_at, updated_at, note, removed_by_id, removed_date, updated_by_id, is_deleted, import_file_id, effective_date";
const observationResponseFields =
  "id, observation_id, observation_template_field_id, response, unit_id";

const verifyToken = require("./authentication").verifyToken;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(fileUpload());

const port = process.env.PORT || 3001;
app.listen(port, () => console.log(`Main Server listening on port ${port}...`));

app.get("/", (req, res) => {
  if (!res.headersSent) res.send("Welcome to Evalue8 CSV Processor v1.00");
});

app.post("/upload", verifyToken, function (req, res) {
  setTimeout(function () {
    if (res.headersSent) return;

    if (!req.files || Object.keys(req.files).length === 0) {
      return res.status(400).send("No files were uploaded.");
    }

    // Last 3 characters of file name, helps identify file type
    filetype = req.files.sampleFile.name.substring(
      req.files.sampleFile.name.length - 3
    );

    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    let sampleFile = req.files.sampleFile;

    if (filetype == "csv") {
      data = sampleFile.data.toString("utf8");
      dataInArray = CSVToArray(data);
      dataInDict = ArrayToDict(dataInArray);
      databaseInsert(dataInDict);
      return res.status(200).send(dataInDict);
    } else if (filetype == "lsx") {
      var obj = xlsx.parse(sampleFile.data); // parses a file
      var rows = [];
      var writeStr = "";
      var sheet = obj[0];
      //loop through all rows in the sheet
      for (var j = 0; j < sheet["data"].length; j++) {
        //add the row to the rows array
        rows.push(sheet["data"][j]);
      }
      //creates the csv string
      for (var i = 0; i < rows.length; i++) {
        writeStr += rows[i].join(",") + "\n";
      }
      dataInArray = CSVToArray(writeStr);
      dataInDict = ArrayToDict(dataInArray);
      databaseInsert(dataInDict)
      return res.status(200).send(dataInDict);
    } else {
      return res.send("Invalid File Type");
    }
  }, 2000);
});

// function for finding keyvalue loop
function findKeyValue(localarr) {
  var Diction = [];
  var currentkey = "";

  for (n = 0; n < localarr.length; n++) {
    var arrN = localarr[n];
    var ifHasValue = 0;
    for (i = 0; i < arrN.length; i++) {
      if (arrN[i] !== "" && !IsValue(arrN[i])) {
        currentkey = arrN[i];
      }
      if (arrN[i] !== "" && IsValue(arrN[i])) {
        Diction.push({ [currentkey]: arrN[i] });
        ifHasValue = 1;
        currentkey = "";
      }
    }
  }
  //  Diction.push({[catogary]:findKeyValue(localarr)})
  return Diction;
}

function IsValue(arr) {
  var localarr = arr;
  //todo: read currency type( "xxx,xxx,xxx,xxx"):
  localarr.replace(",", "");

  return !Number.isNaN(Number(localarr)) || localarr[0] == "$";
}

function IsDatatype(arr) {
  var dataposition =
    arr.indexOf(" Jan ") +
    arr.indexOf(" January ") +
    arr.indexOf(" Feb ") +
    arr.indexOf(" February ") +
    arr.indexOf(" Mar ") +
    arr.indexOf(" March ") +
    arr.indexOf(" Apr ") +
    arr.indexOf(" April ") +
    arr.indexOf(" May ") +
    arr.indexOf(" Jun ") +
    arr.indexOf(" June ") +
    +arr.indexOf(" Jul ") +
    arr.indexOf(" July ") +
    arr.indexOf(" Aug ") +
    arr.indexOf(" August ") +
    arr.indexOf(" Sep ") +
    arr.indexOf(" September ") +
    arr.indexOf(" Oct ") +
    arr.indexOf(" October ") +
    arr.indexOf(" Nov ") +
    arr.indexOf(" November ") +
    arr.indexOf(" Dec ") +
    arr.indexOf(" December ");
  return dataposition > -23;
}

// Ingests array and converts it into KV pairs in a Dict
function ArrayToDict(arr) {
  var localarr = arr;
  var newDic = [];
  var currentkey = "";

  // find title
  for (n = 0; n < localarr.length; n++) {
    var arrN = localarr[n];
    var ifHasValue = 0;
    for (i = 0; i < arrN.length; i++) {
      if (arrN[i] !== "" && !IsValue(arrN[i])) {
        currentkey = arrN[i];
      }
      if (arrN[i] !== "" && IsValue(arrN[i])) {
        ifHasValue = 1;
        currentkey = "";
      }
    }
    if (ifHasValue == 0 && currentkey !== "" && currentkey !== "\ufeff") {
      var title = currentkey;
      localarr.splice(0, n + 1);
      currentkey = "";
      break;
    }
  }
  // find date
  for (n = 0; n < localarr.length; n++) {
    var arrN = localarr[n];
    var ifHasValue = 0;
    for (i = 0; i < arrN.length; i++) {
      if (arrN[i] !== "" && !IsValue(arrN[i])) {
        currentkey = arrN[i];
      }
      if (arrN[i] !== "" && IsValue(arrN[i])) {
        ifHasValue = 1;
        currentkey = "";
      }
    }
    if (
      ifHasValue == 0 &&
      currentkey !== "" &&
      currentkey !== "\ufeff" &&
      IsDatatype(currentkey)
    ) {
      newDic.push({
        title: title,
        date: currentkey,
      });
      localarr.splice(0, n + 1);
      currentkey = "";
      break;
    }
  }
  // find key value with category
  newDic.push(findKeyValue(localarr));
  return newDic;
}

// Converts CSV files into a 2D array
function CSVToArray(strData, strDelimiter) {
  strDelimiter = strDelimiter || ",";

  var objPattern = new RegExp(
    "(\\" +
      strDelimiter +
      "|\\r?\\n|\\r|^)" +
      '(?:"([^"]*(?:""[^"]*)*)"|' +
      '([^"\\' +
      strDelimiter +
      "\\r\\n]*))",
    "gi"
  );

  var arrData = [[]];
  var arrMatches = null;

  while ((arrMatches = objPattern.exec(strData))) {
    var strMatchedDelimiter = arrMatches[1];

    if (strMatchedDelimiter.length && strMatchedDelimiter !== strDelimiter) {
      arrData.push([]);
    }

    var strMatchedValue;

    if (arrMatches[2]) {
      strMatchedValue = arrMatches[2].replace(new RegExp('""', "g"), '"');
    } else {
      strMatchedValue = arrMatches[3];
    }
    // Remove dollar signs if present
    if (
      strMatchedValue[0] == "$" &&
      strMatchedValue[1] >= "0" &&
      strMatchedValue[1] <= "9"
    ) {
      strMatchedValue = strMatchedValue.replace("$", "");
    }

    // Remove commas from numbers
    if (
      strMatchedValue[0] >= "0" &&
      strMatchedValue[0] <= "9" &&
      strMatchedValue[strMatchedValue.length - 1] >= "0" &&
      strMatchedValue[strMatchedValue.length - 1] <= "9"
    ) {
      strMatchedValue = strMatchedValue.replace(/,/g, "");
    }
    // Push into final array
    arrData[arrData.length - 1].push(strMatchedValue);
  }
  return arrData;
}


// Function for inserting dictionary information into Evalue8's database
function databaseInsert(dict) {

  var id = 0;

  findMaxID(0, function (result) {
    if (isNaN(parseInt(Object.values(result[0])))) {
      id = 1;
    }
    else {
      id = parseInt(Object.values(result[0])) + 1;
    }  
    // Inserts reference to the data in the observations table
    pool.query(
      "INSERT INTO observations (" +
        observationFields +
        ") VALUES (" +
        id +
        ", 2, 3, '2019-12-10', '2019-12-11', 5, NOW(), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)",
      function (err, result) {
        if (err) throw err;
        console.log("1 Record Inserted");
      }
    );
  })

  // Inserts specific data from dictionary into observation_repsonses
  var i = 0;
  var observationResponseID = 0;
  findMaxID(1, function(result){
    if (isNaN(parseInt(Object.values(result[0])))) {
      observationResponseID = 1;
    }
    else {
      observationResponseID = parseInt(Object.values(result[0])) + 1;
    }
    while (i < dict[1].length) {
      pool.query(
        "INSERT INTO observation_responses (" +
          observationResponseFields +
          ") VALUES (" +
          observationResponseID +
          ", " +
          id +
          ", 3," +
          Object.values(dict[1][i]) +
          ",168)",
        function (err, result) {
          if (err) throw err;
          console.log("1 Record Inserted");
        }
      );
      i = i + 1;
      observationResponseID = observationResponseID + 1;
    }
  })
}


// Function for checking the next available ID for observations and observation_responses
function findMaxID(IDType, callback) {

  var getMaxObservationID = "SELECT MAX(id) FROM observations"
  var getMaxObservationResponseID = "SELECT MAX(id) FROM observation_responses"

  if (IDType == 0){
    pool.query(getMaxObservationID, function (err, result){
      if (err) console.log(err);
      return callback(result);
    })
  }

  else {
    pool.query(getMaxObservationResponseID, function (err, result){
      if (err) console.log(err);
      return callback(result);
    })
  }
}