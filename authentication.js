
require('dotenv').config();

const express = require('express');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const bcrypt = require('bcrypt');

const app = express();

app.use(bodyParser.json());

const host = process.env.HOST || "localhost";
const user = process.env.USER || "user";
const password = process.env.PASSWORD || "test";

var con = mysql.createPool({
  connectionLimit: 100,
  host: "localhost",
  user: "root",
  password: "password",
  database: "apiDatabase",
  port: "3307"
});

app.post('/login', (req, res) => {
  const userId = req.body.userId
  const userPassword = req.body.userPassword

  const user = {
    userId: userId,
    userPassword: userPassword
  }

  if (typeof userId === 'undefined') return res.sendStatus(400);
  if (typeof userPassword === 'undefined') return res.sendStatus(400);

  // Generate hashed password
  const saltRounds = 10;

  // Check if UserId exists in the database
  var sql = "SELECT UserId, Password FROM users WHERE UserId = '" + userId + "'; ";

  con.query(sql, function (err, result) {
    if (err) return res.sendStatus(500);

    if (result.length == 0) {
      
      //const hashedPassword = bcrypt.hash(userPassword, saltRounds);
      const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET)
      
      jwt.sign({ user }, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '1h' }, (err, accessToken) => {
        if (err) return res.sendStatus(500);
        bcrypt.hash(userPassword, saltRounds, (err, hashedPassword) => {
          if (err) return res.sendStatus(500);
          var name = "evalue8_" + userId;
          var sql = "INSERT INTO users (UserId, Name, Password, AccessToken, AccessTokenExpireTime, RefreshToken) VALUES ('"
            + userId + "', '" + name + "', '" + hashedPassword + "', '" + accessToken + "', NOW() + INTERVAL 1 HOUR, '"
            + refreshToken + "')";
          con.query(sql, userId, function (err, result) {
            if (err) return res.sendStatus(400);
            console.log(result);
            res.json({
              message: "Account created.",
              userId,
              name,
              userPassword,
              accessToken,
              refreshToken
            });
          });
        });
      });

    } else {
      // User already exists, check if password match. If match, sends 409 Conflict. If not, sends 403 Forbidden.
      const existingPassword = result[0].Password;
      bcrypt.compare(userPassword, existingPassword, function(err, isMatch) {
        if (err) return res.sendStatus(500);
        if (!isMatch) {
          return res.sendStatus(403);
        } else {
          return res.sendStatus(409);
        }
      });
    };
  });
})

app.delete('/logout', verifyToken, function (req, res) {

  var userId = req.body.userId

  setTimeout(function () {
    if (!res.headersSent) {
      var sql = "DELETE FROM users WHERE UserId = '" + userId + "';";

      con.query(sql, function (err, result) {
        if (err) return res.sendStatus(500);
        console.log("Record DELETE succeeded.");
        res.sendStatus(204);
        return;
      });
    }
  }, 200);
})

function verifyToken(req, res, next) {
  var userId = req.body.userId
  var accessToken = req.body.accessToken
  var refreshToken = req.body.refreshToken

  if (typeof userId === 'undefined') return res.sendStatus(400);

  jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET, (err, data) => {
    if (err) {

      var sql = "SELECT UserId, AccessToken FROM users WHERE UserId = '" + userId + "' AND AccessToken = '" + accessToken + "' ;";
      con.query(sql, function (err, result) {
        if (err) return res.sendStatus(500);

        // Check if access token is invalid, otherwise it has expired.
        if (typeof result[0] === 'undefined') return res.sendStatus(401);

        console.log("Access token has expired. Checking refresh token now...");

        var sql = "SELECT UserId, RefreshToken FROM users WHERE UserId = '" + userId + "' AND RefreshToken = '" + refreshToken + "' ;";

        con.query(sql, function (err, result) {
          if (err) return res.sendStatus(500);

          // Check if user has valid refresh token
          if (typeof result[0] === 'undefined') return res.sendStatus(401);
          if (!(result[0].RefreshToken == refreshToken)) {
            return res.sendStatus(403);
          } else {
            jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
              if (err) return res.sendStatus(403);

              // If refresh token is valid, generate another access token to replace the expired one
              console.log("RefreshToken is valid for user: " + userId + ", regenerate access token now...");
              jwt.sign({ user }, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '1h' }, (err, accessToken) => {
                if (err) return res.sendStatus(500);

                var sql = "UPDATE users SET AccessToken = '" + accessToken + "', AccessTokenExpireTime = NOW() + INTERVAL 1 HOUR WHERE UserId = '" + userId + "';";
                con.query(sql, function (err, result) {
                  if (err) return res.sendStatus(500);
                  console.log("AccessToken regenerated successfully: " + result);
                  res.json({
                    message: "Previous access token expired, please use the accessToken below.",
                    accessToken
                  })
                });
              });
            })
          }
        });
      });
    } else {
      jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err) => {
        if (err) return res.sendStatus(401);

        var sql = "SELECT UserId, AccessToken, RefreshToken FROM users WHERE UserId = '" + userId + "' AND AccessToken = '" + accessToken + "' AND RefreshToken = '" + refreshToken + "' ;";
        con.query(sql, function (err, result) {
          if (err) return res.sendStatus(500);

          // userId does not match with valid accessToken or refreshToken
          if (typeof result[0] === 'undefined') return res.sendStatus(400);
        });

      })
    }
    next();
  });

}

const port = process.env.AUTHPORT || 5000;
app.listen(port, () => console.log(`Auth server listening on port ${port}...`));

module.exports = {
  verifyToken,
  app
};
