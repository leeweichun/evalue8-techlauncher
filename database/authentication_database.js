// Configuration

// * IMPORTANT:
// Please use the SQL script instead to create the database and the table.
// *

// Create the database
var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "user",
  password: "test",
});

//create database
con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
  con.query("CREATE DATABASE apiDatabase", function (err, result) {
    if (err) throw err;
    console.log("Database created");
  });
});

var connection = mysql.createConnection({
  host: "localhost",
  user: "user",
  password: "test",
  database: "apiDatabase"
});

//create table to store users information
connection.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");

  var sql = "CREATE TABLE users (UserId INT NOT NULL PRIMARY KEY, Name VARCHAR(255) NOT NULL, Password VARCHAR(255) NOT NULL, AccessToken VARCHAR(3000) NOT NULL, AccessTokenExpireTime TIMESTAMP NOT NULL, RefreshToken VARCHAR(3000) NOT NULL)";
  connection.query(sql, function (err, result) {
    if (err) throw err;
    console.log("Auth table created!");
  });
});
